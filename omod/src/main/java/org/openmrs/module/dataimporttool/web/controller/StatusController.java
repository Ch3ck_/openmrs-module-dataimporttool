package org.openmrs.module.dataimporttool.web.controller;


import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.openmrs.api.context.Context;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.openmrs.api.context.Context;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Controller behind the "status.jsp" page. Should list off available urls and representations.
 */
@Controller
@RequestMapping(value="/module/dataimporttool/status.list")
public class StatusController {
	
	protected final Log log = LogFactory.getLog(getClass());

	/** Success form view name */
	private final String SUCCESS_FORM_VIEW = "/module/dataimporttool/status";

	
	/**
	 * Initially called after the formBackingObject method to get the landing form name
	 * 
	 * @return String form view name
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String showForm(ModelMap model) {
		return SUCCESS_FORM_VIEW;
	}
	
}
