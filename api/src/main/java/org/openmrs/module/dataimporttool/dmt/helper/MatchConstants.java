/**
 * The contents of this file are subject to the OpenMRS Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://license.openmrs.org
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * Copyright (C) OpenMRS, LLC.  All Rights Reserved.
 */
 
package org.openmrs.module.dataimporttool.dmt.helper;



public interface MatchConstants {
	//default values
	public static final String NA = "N/A";
	public static final String YES = "YES";
	public static final String NO = "NO";
	public static final String AI = "AI";
	public static final String TOP = "TOP";
	public static final String TOP2 = "TOP2";
	public static final String TOP3 = "TOP3";
	public static final String TOP4 = "TOP4";
	public static final String TOP5 = "TOP5";
	public static final String ALL = "ALL";
	public static final String CURR = "CURR";
	public static final String CURR2 = "CURR2";
	public static final String CURR3 = "CURR3";
	public static final String CURR4 = "CURR4";
	public static final String EQUALS = "EQUALS";
	public static final String AI_SKIP_TRUE = "AI/SKIP/TRUE";
	public static final String AI_SKIP_FALSE = "AI/SKIP/FALSE";
	public static final String SKIP = "SKIP";
	public static final String NULL = "NULL";
	public static final String NOW = "NOW";
	public static final String UNMATCHED = "UNMATCHED";
	//Database datatypes
	public static final String INT = "INT";
	public static final String BOOL = "BOOL";
	public static final String DATE = "DATE";
	public static final String DATETIME = "DATETIME";
	public static final String DOUBLE = "DOUBLE";
	public static final String OR = ">>";

}
