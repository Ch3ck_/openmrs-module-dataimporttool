/**
 * The contents of this file are subject to the OpenMRS Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://license.openmrs.org
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * Copyright (C) OpenMRS, LLC.  All Rights Reserved.
 */
package org.openmrs.module.dataimporttool.api.db;

import org.openmrs.module.dataimporttool.DataImportTool;
import org.hibernate.SessionFactory;

import java.util.List;

import org.openmrs.api.db.DAOException;
import org.springframework.transaction.annotation.Transactional;

/**
 *  Database methods for {@link DataImportToolService}.
 * 
 * This class is used to manipulate SQL-based databases using SQL native queries
 *
 */
@Transactional
public interface DataImportToolDAO {

	/**
	 * getSessionFactory
	 *
	 */
	public SessionFactory getSessionFactory();

	/**
	 * runUpdateQuery
	 *
	 * @param queryString
	 */
	public int runUpdateQuery(String queryString);

	/**
	 * runListQuery, executes the SQL queries on the list
 	 *
	 * @param queryString
	 * @return List
	 */
	public List runListQuery(String queryString);


	 /**
          * @see org.openmrs.module.dataimporttool.api.DataImportToolService#getAllDataImportTools()
          */
        public List<DataImportTool> getAllDataImportTools();
        /**
         * @see org.openmrs.module.dataimporttool.api.DataImportToolService#getDataImportTool(java.lang.Integer)
	 */
        public DataImportTool getDataImportTool();
        /**
         * @see org.openmrs.module.dataimportool.api.DataImportToolService#saveDataImportTool(org.openmrs.module.dataimporttool.DataImportTool)
         */
        public DataImportTool saveDataImportTool(DataImportTool dit);
        /**
         * @see org.openmrs.module.dataimportool.api.DataImportToolService#purgeDataImportTool(org.openmrs.module.dataimporttool.DataImportTool)
         */
        void purgeDataImportTool(DataImportTool dit);
}
