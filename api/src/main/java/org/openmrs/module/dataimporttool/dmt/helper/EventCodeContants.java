/**
 * The contents of this file are subject to the OpenMRS Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://license.openmrs.org
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * Copyright (C) OpenMRS, LLC.  All Rights Reserved.
 */

package org.openmrs.module.dataimporttool.dmt.helper;

/**
 * Event Code Constants
 *
 */
public interface EventCodeContants {
	//WARNING
	public static final String WAR001 = "WAR001";
	public static final String WAR002 = "WAR002";
	//ERROR
	public static final String ERR001 = "ERR001";
	public static final String ERR002 = "ERR002";
	public static final String ERR003 = "ERR003";
	public static final String ERR004 = "ERR004";
	public static final String ERR005 = "ERR005";
	public static final String ERR006 = "ERR006";
	public static final String ERR007 = "ERR007";
	public static final String ERR008 = "ERR008";
	public static final String ERR009 = "ERR009";
	public static final String ERR010 = "ERR010";
	public static final String ERR011 = "ERR011";
	public static final String ERR012 = "ERR012";
	public static final String ERR013 = "ERR013";
	public static final String ERR014 = "ERR014";
	public static final String ERR015 = "ERR015";
	public static final String ERR016 = "ERR016";
	public static final String ERR017 = "ERR017";
	public static final String ERR018 = "ERR018";
	public static final String ERR019 = "ERR019";
	//INFO
	public static final String INF001 = "INF001";
	public static final String INF002 = "INF002";
	public static final String INF003 = "INF003";
	public static final String INF004 = "INF004";
	public static final String INF005 = "INF005";
	public static final String INF006 = "INF006";
	public static final String INF007 = "INF007";
	public static final String SEPARATOR = "common.separator";
}
